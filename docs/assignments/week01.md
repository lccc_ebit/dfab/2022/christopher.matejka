# 1. Assignment 1

This is my first assignment of the year, and as such I am documenting how to use git.
## Getting Started
- [Git For Windows](https://gitforwindows.org/)
- [Setup Guide](https://fabacademy.org/2019/docs/FabAcademy-Tutorials/week01_principles_practices_project_management/git_simple.html)

## 2. Explanation

To start, download git from the first link (assuming your account has already been created). After that, you will enter username and email via the git config commands `–-global user.name YOUR_USERNAME` and `-–global user.email jSmith@mail.com` respectively.   
After that, check to see if you've got a viya `cat ~/.ssh/id_rsa.pub`, codes can generated by using the command `ssh-keygen -t rsa -C "$your_email" `and can be viewed with `cat ~/.ssh/id_rsa.pub`. Copy your key using `clip  ~/.ssh/id_rsa.pub` and enter onto the web version of GIT.   
With the software installed, now for a basic rundown of how to use GIT. First, create a folder to clone your repository to (you can also create your own but for the sake of this assignment I'll just go with cloning).   
The command for cloning the student repo would be `git clone git@git.fabacademy.org:fabacademy2017/yourlabname/yourstudentnumber.git`   
With your repo cloned, here are some essential commands, which I'll copy and paste from the setup guide.





## Essential Commands 

    1. Add the new files you added to git

        git add index.html to upload file by file
        git add . to upload all the files at once

    2. To download the last copy from the repository

        git pull (for downloading the last copy of the repository)

    3. To have a working copy of your repo

        git merge (to have a working copy)

    4. Now name your update, so you know what you changed with this push.

        git commit -m change you did

    5. Upload to the Repository

        git push 
For example, this page was made by editing the week 1 file in notepad, saving it, entering the command git add assignments/week01.md into the GIT GUI, git commit -m done and then typing git push to confirm everything.

## Commands also of note:   
git rm: removes a file from the repository    
git mv: move or rename a file ,a directory or a symlinkgit   
git status: to see what's been changed.   


 git rm -rf folder-name: To delete an entire folder.

## Links to help with Markdown stuff

- [Markdown Cheat Sheet](https://www.markdownguide.org/cheat-sheet/)
- [Markdown Basic Syntax](https://www.markdownguide.org/basic-syntax/)
- [Markdown](https://en.wikipedia.org/wiki/Markdown)
