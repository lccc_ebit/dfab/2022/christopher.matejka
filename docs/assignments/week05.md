# 5. Cutting and soddering the first board

I will be documenting how I cut and soldered my board modsproject.org Modela MDX-50

## Setup (modsproject.org)
(Be sure to have the trace and interior pics ready before this) 
Go to modsproject.org, right click to pull up the menu and click "programs", "open programs", and then finally "mill 2D PCB", which will be in the G-code section

Select the PNG of either the trace or the interior, and make sure the mill settings are at 1/64 and 1/32 respectively. Make sure they're both set to do 4 passes and (very important) **have it so the model is set to MDX-40, not 20**!

After getting the desired settings press calculate to get your file (for each file), which takes us to the next step.
## Cutting The Board (MDX-50)
Firstly, check to make sure the machine has a sacirifical layer.   
Using 3 strips of double sided tape, ensure that board is in secured and at a good 90 degree angle.
Before cutting, the machine needs to be setup. Make sure its powered on, then begin honing the X and Y axises. When you reach close to the bottom left of where you're cutting with each axis, hold the origin button to set the X0 and Y0 respectively.
The Z axis requires its own bit of machinery to be honed and will need to have it done for each part. (Tool 1 is 1/64, tool 2 is 1/32)
Plug the z-axis thing into the outlet inside the cutbed, then place it on top of the board you're cutting. Move the tool over it and press the z-axis oirgin button thing (rewrite this). With all the axises set, you can move onto cutting.

After the axises have been honed, put the USB drive in the mill's PC and go to the cutting software (right more about this later) and start the trace file you got from modsproject and make sure tool 1 is chosen.
Note: make sure you only select one file, otherwise all of them will cut without a tool change.

After the trace is cut, change to tool 2 and redo the z axis honing and run the interior cut file, then remove the board when the machine is done.
## Soldering
After the board is cut, we move onto soldering. Not much to this except making sure the parts and pins match up to the diagram, which I'll post.

insert pics from phone here
Before you actually solder a component, make sure there's ample flux applied to the area of the board. 
After applying flux, hold the iron to a connector on one of the parts so it doesn't move around as much before using some actual solder on the other parts.
Use the soldering iron to melt wire to put the components in place

After this, that's the end.

#Photos
![](./images/firstboard.jpg)